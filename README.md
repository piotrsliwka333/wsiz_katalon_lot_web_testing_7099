# wsiz_katalon_lot_web_testing_7099


## Testy dla aplikacji Schedule Your Meal przy wykorzystaniu Katalon. 

Testy stworzene na potrzeby przedmiotu Testowanie Opgrogramowania


## Rozpiska tetstów

- [ ] Add_recipe - test sprawdzając poprawność dodawania przepisów do aplikacji
- [ ] Add_shopping_product - test sprawdzając poprawność dodawnia produktu do listy zakupów


## Link do aplikacji
[ScheduleYourMeal](https://schedule-your-meal.web.app/)
