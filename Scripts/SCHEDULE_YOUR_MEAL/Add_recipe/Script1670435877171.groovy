import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.main_page_url)

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/LOGIN/login_input'), email)

WebUI.setEncryptedText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/LOGIN/password_input'), password)

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/LOGIN/login_button'))

WebUI.waitForPageLoad(3)

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/DESKTOP/close_app_tutorial_button'))

WebUI.waitForPageLoad(3)

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/DESKTOP/recipe_navigation_link'))

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/add_new_recipe_button'))

/*
 Walidacja
*/

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/save_recipe_button'))

String recipe_name_input_error_message = WebUI.getText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_name_input_error'))
String recipe_description_textarea_error_message = WebUI.getText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_description_textarea_error'))
String recipe_instruction_textarea_error_message = WebUI.getText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_instruction_textarea_error'))
String recipe_ingredient_input_error_message = WebUI.getText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_ingredient_input_error'))

assert recipe_name_input_error_message == recipe_name_input_error_expected_message
assert recipe_description_textarea_error_message == recipe_description_textarea_error_expected_message
assert recipe_instruction_textarea_error_message == recipe_instruction_textarea_error_expected_message
assert recipe_ingredient_input_error_message == recipe_ingredient_input_expected_message

WebUI.setText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_name_input'), recipe_name)
WebUI.setText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_description_textarea'), recipe_description)

for (def row = findTestData('Instructions').getRowNumbers(); row > 0; row--) {
	WebUI.setText(
		findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_instruction_textarea'),
		findTestData('Instructions').getValue('instruction', row)
		)
	WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/add_instruction_button'))	
}

for (def row = 1; row <= findTestData('Ingredients').getRowNumbers(); row++) {
	WebUI.setText(
		findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipe_ingredient_input'),
		findTestData('Ingredients').getValue('ingredient', row)
		)
	WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/add_ingredient_button'))
}

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/save_recipe_button'))

String list_title_text = WebUI.getText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/RECIPES/recipes_list_title'));

//Check if user was correctly moved to recipes list section after adding new recipe
assert list_title_text == recipes_list_title

WebUI.closeBrowser();

