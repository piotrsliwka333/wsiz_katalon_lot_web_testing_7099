import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement

WebUI.openBrowser(GlobalVariable.main_page_url);

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/LOGIN/login_input'), GlobalVariable.email)
WebUI.setEncryptedText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/LOGIN/password_input'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/LOGIN/login_button'))

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/DESKTOP/close_app_tutorial_button'))

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/DESKTOP/shopping_navigation_link'))

WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/SHOPPING/add_shopping_product_button'))

String product_name_input_error_text = WebUI.getText(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/SHOPPING/product_name_input_error'))
assert product_name_input_error_text == product_name_input_error_expected_text

TestObject shoppingItemBeforePushToList = findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/SHOPPING/shopping-list_element')
List<WebElement> shoppingItemsBeforePushToList = WebUI.findWebElements(shoppingItemBeforePushToList, 10)

for (def row = 1; row <= findTestData('Ingredients').getRowNumbers(); row++) {
	WebUI.setText(
		findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/SHOPPING/product_name_input'),
		findTestData('Ingredients').getValue('ingredient', row)
		)
	WebUI.click(findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/SHOPPING/add_shopping_product_button'))
	WebUI.delay(1)
}

TestObject shoppingItemAfterPushToList = findTestObject('Object Repository/SCHEDULE_YOUR_MEAL/SHOPPING/shopping-list_element')
List<WebElement> shoppingItemsAfterPushToList = WebUI.findWebElements(shoppingItemBeforePushToList, 10)
assert shoppingItemsAfterPushToList.size() == shoppingItemsBeforePushToList.size() + 5

WebUI.closeBrowser()

