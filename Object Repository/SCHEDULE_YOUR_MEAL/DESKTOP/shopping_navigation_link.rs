<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>shopping_navigation_link</name>
   <tag></tag>
   <elementGuidId>91b1db27-a4cb-4e2b-ba32-4b0842fdc100</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div > a[href=&quot;/schedule-your-meal/shopping&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
