<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Link który przenosi nas na stronę z przepisami</description>
   <name>recipe_navigation_link</name>
   <tag></tag>
   <elementGuidId>e616f690-8961-4da6-868c-47e4614535e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div[class=&quot;desktop__button-container&quot;] > a[href=&quot;/schedule-your-meal/recipes&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
