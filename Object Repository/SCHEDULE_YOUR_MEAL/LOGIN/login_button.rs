<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>login_button</name>
   <tag></tag>
   <elementGuidId>4b07e0fa-d5db-4307-bd26-f382f5fc5a1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>form[class=&quot;login-form&quot;] > button[class=&quot;login-form__button&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
